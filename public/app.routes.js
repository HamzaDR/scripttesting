angular.module("app")
    .config(config)

function config($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('app.dashboard.third', {
            url: '/third',
            views: {
                'dashboard_details': {
                    templateUrl: 'features/dashboard/third/main-view.html?v1.4.0_165',
                    controller: 'ThirdCtrl'
                }
            }
        })
        .state('app.dashboard.second', {
            url: '/second',
            views: {
                'dashboard_details': {
                    templateUrl: 'features/dashboard/second/main-view.html?v1.4.0_165',
                    controller: 'SecondCtrl'
                }
            }
        })
        .state('app.dashboard.first', {
            url: '/first',
            views: {
                'dashboard_details': {
                    templateUrl: 'features/dashboard/first/main-view.html?v1.4.0_165',
                    controller: 'FirstCtrl'
                }
            }
        })
        .state('app.dashboard.calendar', {
            url: '/calendar',
            views: {
                'dashboard_details': {
                    templateUrl: 'features/dashboard/calendar/main-view.html?v1.4.0_165',
                    controller: 'CalendarCtrl'
                }
            }
        })
        .state('app.dashboard', {
            url: '/dashboard',
            views: {
                'menuContent': {
                    templateUrl: 'features/dashboard/main-view.html?v1.4.0_165',
                    controller: 'DashboardCtrl'
                }
            },
            resolve: {
                checkAuth: checkAuth,
                checkRoles: checkRoles,
                airportsData: resolve_airports
            }
        })
        .state('app.myOffers', {
            url: '/myOffers/:type/:histo',
            cache: false,
            views: {
                'menuContent': {
                    templateUrl: 'features/myOffers/main-view.html?v1.4.0_165',
                    controller: 'MyOffersCtrl'
                }
            },
            resolve: {
                checkRefs: checkRefs,
                checkAuth: checkAuth,
                checkRoles: checkRoles
            }
        })
        .state('app.editAirport', {
            url: '/editAirport',
            cache: false,
            views: {
                'menuContent': {
                    templateUrl: 'features/editAirport/main-view.html?v1.4.0_165',
                    controller: 'EditAirportCtrl'
                }
            },
            resolve: {
                checkRefs: checkRefs,
                checkAuth: checkAuth,
                checkRoles: checkRoles
            }
        })
        .state('app.home', {
            url: '/home/:type/:nb',
            cache: true,
            views: {
                'menuContent': {
                    templateUrl: 'features/home/main-view.html?v1.4.0_165',
                    cache: true,
                    controller: 'HomeCtrl'
                }
            },
            resolve: {
                checkRefs: checkRefs,
                checkAuth: checkAuth,
                checkRoles: checkRoles
            }
        })
        .state('app.offerValidated', {
            url: '/offerValidated/:offer_id',
            cache: true,
            views: {
                'menuContent': {
                    templateUrl: 'features/home/offer-validated.html?v1.4.0_165',
                    cache: true,
                    controller:'OfferValidatedCtrl'
                }
            },
            resolve: {
                checkRefs: checkRefs,
                checkAuth: checkAuth,
                checkRoles: checkRoles
            }
        })
        .state('app.home2', {
            url: '/offer/:offer_id',
            cache: false,
            views: {
                'menuContent': {
                    templateUrl: 'features/home/main-view.html?v1.4.0_165',
                    cache: true,
                    controller: 'HomeCtrl'
                }
            },
            resolve: {
                checkRefs: checkRefs,
                checkAuth: checkAuth,
                checkRoles: checkRoles,
                checkOffer: checkOffer
            }
        })
        .state('app', {
            url: '/app',
            abstract: false,
            controller: "MenuCtrl",
            templateUrl: 'features/menu/main-view.html?v1.4.0_165'
        })
        .state('login',{
            url: '/login',
            templateUrl: 'features/login/main-view.html?v1.4.0_165',
            controller: "LoginCtrl"
        })
        .state('loginForgotPassword', {
            url: '/loginForgotPassword',
            templateUrl: 'features/login/loginForgotPassword.html?v1.4.0_165',
            controller: "LoginCtrl"
        })
        .state('loginChangePassword', {
            url: '/loginChangePassword/:email/:temp_password',
            templateUrl: 'features/login/loginChangePassword.html?v1.4.0_165',
            controller: "LoginCtrl"
        })
        .state('loading', {
            url: '/loading',
            cache:false,
            templateUrl: 'features/login/loading.html?v1.4.0_165',
            controller: "LoadingCtrl"
            ,resolve: {
                checkRoles: checkRoles
            }
        })

    $urlRouterProvider.otherwise('/loading');

    function resolve_airports(refs) {
        return refs.resolve_data('airports');
    }

    function checkOffer($auth, $rootScope, $q, offers, $stateParams, $location,simpleDialog,roleService) {
        var deferred = $q.defer();
        // if ($rootScope.currentOffer && $rootScope.currentOffer.id == $stateParams.offer_id) {
        //     deferred.resolve();
        // } else {
        return offers.get($stateParams.offer_id).then(function(r) {
            console.log('loaded '+$stateParams.offer_id);
            $rootScope.currentOffer = r.data;
            if(r.data.offer_airports_am && $rootScope.user.b_am){
                $rootScope.currentOffer.offer_airports = r.data.offer_airports_am;
            }
        }, function(r){
            if(r.status == 404){
                $rootScope.currentState = 'app.myOffers';
                roleService.applyRole();
                simpleDialog.show(r);
            } else {
                $location.path('/login');
            }
            // $location.path('/login');
        });
        // }
        // return deferred.promise;
    }

    /**
     * Check if authenticated, otherwise redirect to login
     * **/
    function checkAuth($auth, $location, $q) {
        var deferred = $q.defer();
        if (!$auth.isAuthenticated()) {
            window.redirectAfterLogin = location.hash;
            deferred.reject();
            $location.path('/login');
        } else {
            deferred.resolve();
        }
        return deferred.promise;
    }

    /**
     * Get ref datas if not loaded yet
     * **/
    function checkRefs($rootScope, $location, $q, refs, $auth) {
        var deferred = $q.defer();
        if ($rootScope.refs) {
            deferred.resolve();
        } else { // refs not loaded yet, load it!
            return refs.all()
                .then(function(res) {
                    $rootScope.refs = res.data;
                });
        }
        return deferred.promise;
    }
    /**
     * Get user's roles and apply last one or first one
     * **/
    function checkRoles($rootScope, $q, roleService, $localForage,$auth) {
        var deferred = $q.defer();
        if ($rootScope.roles && $rootScope.roles.length) {
            deferred.resolve();
        } else if(!$auth.isAuthenticated()){
            deferred.resolve();
        }  else if(!$rootScope.roles.length){
            $rootScope.roles = [];
            return $localForage.getItem(['user', 'user_role']).then(function(r) {
                var USER = r[0];
                $rootScope.roles = [];
                $rootScope.user = USER || {};
                if ($rootScope.user.b_ism || $rootScope.user.b_admin) {
                    $rootScope.roles.push('ISM');
                }
                if ($rootScope.user.b_am || $rootScope.user.b_admin) {
                    $rootScope.roles.push('AM');
                }
                if ($rootScope.user.b_credit || $rootScope.user.b_admin) {
                    $rootScope.roles.push('CREDIT_CONTROLLER');
                }
                if ($rootScope.user.b_chef && $rootScope.user.b_ism || $rootScope.user.b_admin) {
                    $rootScope.roles.push('ISM_CHIEF');
                }
                if ($rootScope.user.b_chef && $rootScope.user.b_am || $rootScope.user.b_admin) {
                    $rootScope.roles.push('AM_CHIEF');
                }
                if($rootScope.roles.length>1){
                    var ROLE = r[1] || $rootScope.roles[0];
                } else {
                    var ROLE = $rootScope.roles[0];
                }

                roleService.applyRole(ROLE);
            });
        }
        return deferred.promise;
    }
};
